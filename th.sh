#!/bin/bash
#This script is used with xargs for batch work.
echo 
echo  "**********Runing Tophat with Homemade Bash Script********"

name=$1
data=/home/fattest/Documents/publicdb/Ol_Transcriptome
outp=/home/fattest/Documents/CJB/tophat_out/$name/
indexdir=/home/fattest/Documents/Test/
index=genome
gtf=geneswm.gtf
reads=$data/"$name"_1.fastq
reads2=$data/"$name"_2.fastq

tophat -p 35 -o $outp -G $indexdir$gtf $indexdir$index $reads $reads2 

echo "
name=$1
data=/home/fattest/Documents/publicdb/Ol_Transcriptome
outp=/home/fattest/Documents/CJB/tophat_out/$name/
indexdir=/home/fattest/Documents/Test/
index=genome
gtf=geneswm.gtf
reads=$data/"$name"_1.fastq
reads2=$data/"$name"_2.fastq

tophat -p 35 -o $outp -G $indexdir$gtf $indexdir$index $reads $reads2 
" > $outp/RunLog.txt

