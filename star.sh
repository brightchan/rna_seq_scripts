#!/bin/bash
#This script is used with xargs for batch work. 
echo 
echo  "**********Runing STAR with Homemade Bash Script********"

name=$1
data=/home/fattest/Documents/publicdb/Ol_Transcriptome
outp=/home/fattest/Documents/CJB/star_out/"$1"_star
indexDir=/home/fattest/Documents/Test/STAR_ind
gtf=/home/fattest/Documents/Test/geneswm.gtf
reads=$data/"$1"_1.fastq
reads2=$data/"$1"_2.fastq

mkdir $outp
STAR --genomeDir $indexDir --runThreadN 25 --sjdbGTFfile $gtf -genomeLoad LoadAndKeep --outSAMstrandField intronMotif --outFilterIntronMotifs RemoveNoncanonical --outSAMtype BAM SortedByCoordinate --readFilesIn $reads $reads2 --outFileNamePrefix $outp/ 
#--outSAMstrandField intronMotif --outFilterIntronMotifs RemoveNoncanonical is for Cufflink

echo " 
name=$1
data=/home/fattest/Documents/publicdb/Ol_Transcriptome
outp=/home/fattest/Documents/CJB/star_out/"$1"_star
indexDir=/home/fattest/Documents/Test/STAR_ind
gtf=/home/fattest/Documents/Test/geneswm.gtf
reads=$data/"$1"_1.fastq
reads2=$data/"$1"_2.fastq

mkdir $outp
STAR --genomeDir $indexDir --runThreadN 25 --sjdbGTFfile $gtf -genomeLoad LoadAndKeep --outSAMstrandField intronMotif --outFilterIntronMotifs RemoveNoncanonical --outSAMtype BAM SortedByCoordinate --readFilesIn $reads $reads2 --outFileNamePrefix $outp/" > $outp/RunLog.txt

