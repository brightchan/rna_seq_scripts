#!/bin/bash

outp="/home/fattest/Documents/CJB/blast"
subject="/home/fattest/Documents/Test/merged_19all/merged.fa"
#subject="/home/fattest/Documents/CJB/cuffmerge_out/SRR1524271-81_tophat/merged.fa"
query="/home/fattest/Documents/CJB/blast/afp4a1.fa"


blastn -query $query -subject $subject -out $outp/afp4a1_merged19all_blast_out -outfmt "6 sseqid sgi sacc length pident" -num_threads 20
