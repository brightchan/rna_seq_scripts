#!/bin/bash
#This script is used with xargs for batch work

gtf=~/Documents/Test/geneswm.gtf
outp=~/Documents/CJB/cuffquant_out/HX_Stage10
mask=/home/fattest/Documents/Test/mask.gtf
data=~/Documents/Yunzhi/$1_tp/accepted_hits.bam

mkdir $outp/$1
cuffquant -p 35 -o $outp/$1/ -u -M $mask $gtf $data
date > $outp/$1/RunLog.txt
cat ./cuffquant.sh >> $outp/$1/RunLog.txt #need to mannually add runlog

