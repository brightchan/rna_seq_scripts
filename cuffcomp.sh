#!/bin/bash

outp="/home/fattest/Documents/CJB/cuffcomp_out/SRA176467star_vs_merged19"
inp="/home/fattest/Documents/CJB/cuffmerge_out/SRR1524271-81_star/transcripts.gtf"
inp2="/home/fattest/Documents/Test/merged_19all/merged.gtf"
mkdir $outp
cuffcompare -o $outp/ -C $inp $inp2

cp ./cuffcomp.sh $outp/Runlog.txt
