#!/bin/bash
#This script is used with xargs for batch work. 
echo
echo "*******************Runing Cufflinks with Homemade Script***********************"

name=$1
bam=/home/fattest/Documents/CJB/tophat_out/"$name"/accepted_hits.bam
mkdir /home/fattest/Documents/CJB/cufflink_out/Tophat/"$name"
outp=/home/fattest/Documents/CJB/cufflink_out/Tophat/"$name"
gtf=/home/fattest/Documents/Test/geneswm.gtf
mask=/home/fattest/Documents/Test/mask.gtf
genome=/home/fattest/Documents/Test/genome.fa

cufflinks -p 35 -u -o $outp -L $name -g $gtf -M $mask $bam 

date > $outp/RunLog.txt #add runlog manually.
echo "
name=$1
bam=/home/fattest/Documents/CJB/tophat_out/"$name"/accepted_hits.bam
mkdir /home/fattest/Documents/CJB/cufflink_out/Tophat/"$name"
outp=/home/fattest/Documents/CJB/cufflink_out/Tophat/"$name"
gtf=/home/fattest/Documents/Test/geneswm.gtf
mask=/home/fattest/Documents/Test/mask.gtf
genome=/home/fattest/Documents/Test/genome.fa
cufflinks -p 35 -u -o $outp -L $name -g $gtf -M $mask $bam ">> $outp/RunLog.txt
