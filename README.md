# Workflow Instructions #

For tophat, STAR, cufflink, and cuffquant, use the following command with 'list.txt' containing the prefix of files:

cat list.txt|xargs -n 1 -P 1 ./'command'.sh

For cuffmerge, generate a 'assembly_list.txt' file in the destination folder, then run 'cuffmerge.sh'.

For cuffdiff, generate a 'filelist.txt' and run the 'cuffdiff.sh'. Generation script can also be found in the file.

'parallel_coms' help run multiple blast or other commands parallelly. 
'list_gen.sh' generate list for batch wget download.