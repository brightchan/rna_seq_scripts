#!/bin/bash
#This script is used with xargs for batch work. 

echo
echo "*******************Runing Cuffdiff with Homemade Script***********************"

outp="/home/fattest/Documents/CJB/cuffdiff_out/Resist_Suscept_naturalhaploid"
gtf="/home/fattest/Documents/Test/geneswm.gtf"

#cat /home/fattest/Documents/CJB/list | while read line; do
#  echo "/home/fattest/Documents/CJB/cuffquant_out/$line/abundances.cxb" >> ./filelist
#  done
#
#find '/home/the directory of .cxb files/' -name *.cxb > ./filelist
#
mkdir $outp/
cat ./filelist.txt | xargs cuffdiff -p 35 -o $outp/ -u $gtf -L Resist,Suscept    


echo "******************* Done **********************"
